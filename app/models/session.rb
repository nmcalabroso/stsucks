class Session < ActiveRecord::Base
  has_many :questions
  has_many :answers, dependent: :destroy
  serialize :questionIDs
end