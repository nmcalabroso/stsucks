class Question < ActiveRecord::Base
	has_many :choices
	has_one :answer
	validates :question_body, presence: true
    
	def Question.random_question
		@randarray = Array.new(10)
		(1..10).each do |i|
			@randnum = rand(Question.all.length) + Question.first.id
			while Question.all.length >= 10 and @randarray.detect {|j| j == @randnum } do
				@randnum = rand(Question.all.length) + Question.first.id
			end
			@randarray[i-1] = @randnum
		end

		return @randarray
	end

end
