class Choice < ActiveRecord::Base
	belongs_to :question
	validates :BrA, presence: true
	validates :BrB, presence: true
	validates :BrC, presence: true
	validates :BrD, presence: true
	validates :BrE1, presence: true
	validates :BrE2, presence: true
end
