module StaticPagesHelper

  def full_title(title)
    main_title = 'STSucks'
    if title
      "#{main_title} | #{title}"
    else
      main_title
    end
  end
end
