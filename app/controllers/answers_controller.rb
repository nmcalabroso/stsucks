class AnswersController < ApplicationController
  def create
    @session = Session.find(params[:id])
    @question = Question.find(params[:question_id])
    @answer = Answer.new
    @answer.pick = params["question_#{@question.id.to_s}"].to_i
    @answer.question_id = @question.id
    @answer.session_id = @session.id
    @answer.save
    #binding.pry
    redirect_to question_session_path(@session, @question)
  end

  def update
    @session = Session.find(params[:id])
    @question = Question.find(params[:question_id])
    @answer = Answer.find(@question.answer.id)
    @answer.pick = params["question_#{@question.id.to_s}"].to_i
    @answer.save
    binding.pry
    redirect_to question_session_path(@session, @question) 
  end
  private
    def answer_params
      params.require(:answer).permit(:question_id, :id)
    end
end