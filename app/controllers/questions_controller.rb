class QuestionsController < ApplicationController
  def new
    @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    @question.save
    redirect_to @question
  end

  def index
  end

  def show
    @question = Question.find(params[:id])
    #@randquestion = Question.random_question #<-- Array of Question IDs
  end

  def show_form
    @session = Session.find(params[:id])
    @question = Question.find(params[:question_id])
    @choice = @question.choices.find(params[:question_id])

    #@randquestion = Question.random_question
=begin
    @show_question_array = Array.new(10)
    (1..10).each do |i|
      @show_question_array[i-1] = @session.questions[i-1].id
    end
=end
  end

  def view_result

    @tally = {BracketA: 0, BracketB: 0, BracketC: 0, BracketD: 0, BracketE1: 0, BracketE2: 0}
    @session = Session.find(params[:id])
    @session.answers.each do |answer|
      if answer.pick == 1
        @tally[:BracketA] += 1
      elsif answer.pick == 2
        @tally[:BracketB] += 1
      elsif answer.pick == 3
        @tally[:BracketC] += 1
      elsif answer.pick == 4
        @tally[:BracketD] += 1
      elsif answer.pick == 5
        @tally[:BracketE1] += 1
      elsif answer.pick == 6
        @tally[:BracketE2] += 1
      end  
    end
    @session.destroy
    binding.pry
  end

  private
    def question_params
      params.require(:question).permit(:question_body)
    end
end
