class SessionsController < ApplicationController
  def new

  end

  def create
    if Question.all.length >= 10
      @session = Session.new

      @questionIDs = Question.random_question
      @session.questionIDs = @questionIDs
      #binding.pry
      @questionList = Array.new(10)
      (1..10).each do |i|
        @questionList[i-1] = Question.find(@questionIDs[i-1])
      end

      @session.questions = @questionList

      @session.save
      

      redirect_to question_session_path(@session, @session.questionIDs[0])
    else
      redirect_to new_question_path
    end
  end

  def show
    @session = Session.find(params[:id])
    #binding.pry
  end

  def destroy
  end

end
