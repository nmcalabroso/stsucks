class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.text :questionIDs    
      t.string :result
    end
  end
end
