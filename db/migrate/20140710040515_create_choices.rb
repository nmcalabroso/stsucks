class CreateChoices < ActiveRecord::Migration
  def change
    create_table :choices do |t|
      t.string :BrA
      t.string :BrB
      t.string :BrC
      t.string :BrD
      t.string :BrE1
      t.string :BrE2
      t.references :question, index: true
      t.timestamps
    end
  end
end
