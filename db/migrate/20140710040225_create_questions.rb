class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :question_body
      t.references :session, index: true
      t.timestamps 
    end
  end
end
