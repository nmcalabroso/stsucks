class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.integer :pick
      t.references :question
      t.references :session
    end
  end
end
