Rails.application.routes.draw do

  get 'apply/question'

  root 'static_pages#home'

  #Static Pages
  get 'home' => 'static_pages#home'
  get 'homie' => 'static_pages#homie'
  get 'result' => 'static_pages#result'

  resources :sessions, only: [:new, :create, :destroy, :show] do
    member do
      get 'question/:question_id' => 'questions#show_form', as: :question 
      post 'question/:question_id/answer' => 'answers#create', as: :answer
      patch 'question/:question_id/edit_answer' => 'answers#update', as: :edit_answer
      get 'result' => 'questions#view_result', as: :result
    end
  end 

  get 'sessions/apply_form'

  resources :submit_questions
  resources :questions do
    resources :choices
  end

  #post 'answers/create' => 'answers#create'
 

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
